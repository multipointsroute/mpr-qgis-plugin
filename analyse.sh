#! /bin/bash

export SOURCES=mpr
export PROJECT=mpr-qgis-plugin

sonar-scanner \
  -Dsonar.projectName="$PROJECT" \
  -Dsonar.projectKey="$PROJECT" \
  -Dsonar.projectVersion="$PLUGIN_VERSION" \
  -Dsonar.login="$SONAR_TOKEN" \
  -Dsonar.sources="$SOURCES" \
  -Dsonar.exclusions="$SOURCES/resources*.py" \
  -Dsonar.qualitygate.wait=true
