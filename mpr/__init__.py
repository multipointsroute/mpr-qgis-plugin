__name__ = "mpr"

from qgis.gui import QgisInterface


# noinspection PyPep8Naming
def classFactory(iface: QgisInterface):
    from mpr.mpr import MultiPointsRoutePlugin

    return MultiPointsRoutePlugin(iface)
