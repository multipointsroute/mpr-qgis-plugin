from typing import List

from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QColor, QKeyEvent
from qgis.core import (
    QgsCoordinateReferenceSystem,
    QgsCoordinateTransform,
    QgsPointXY,
    QgsProject,
)
from qgis.gui import (
    QgisInterface,
    QgsMapMouseEvent,
    QgsMapToolEmitPoint,
    QgsRubberBand,
    QgsVertexMarker,
)

from mpr.utils import MprLogger

CRS = QgsCoordinateReferenceSystem("EPSG:4326")


class LineMapTool(QgsMapToolEmitPoint):

    drawing: bool
    points: List[QgsPointXY]

    rubberband: QgsRubberBand
    transform: QgsCoordinateTransform

    draw_finished = pyqtSignal(bool)

    def __init__(self, iface: QgisInterface):
        self.iface = iface
        canvas = iface.mapCanvas()
        QgsMapToolEmitPoint.__init__(self, canvas)

        self.rubberband = QgsRubberBand(canvas, False)
        self.rubberband.setColor(QColor("#1E90FF"))
        self.rubberband.setWidth(4)

        self.sketch_rubber_band = QgsRubberBand(canvas, False)
        self.sketch_rubber_band.setLineStyle(Qt.DotLine)
        self.sketch_rubber_band.setColor(QColor("#1E90FF"))
        self.sketch_rubber_band.setWidth(3)

        self.transform = QgsCoordinateTransform(CRS, canvas.mapSettings().destinationCrs(), QgsProject.instance())
        self.reset()

    def deactivate(self):
        self.canvas().scene().removeItem(self.rubberband)
        self.canvas().scene().removeItem(self.sketch_rubber_band)
        self.clean()

    def reset(self) -> None:
        self.rubberband.reset()
        self.points = []

    def keyPressEvent(self, event: QKeyEvent) -> None:
        if event.key() == Qt.Key_Escape:
            self.iface.actionPan().trigger()

    def on_new_point(self, point: QgsPointXY) -> None:
        # transform points coordinates to WGS84
        p = self.transform.transform(point, QgsCoordinateTransform.ReverseTransform)
        # add transformed point to list and rubberbands
        self.points.append(p)
        self.rubberband.addPoint(point)

    def canvasReleaseEvent(self, event: QgsMapMouseEvent) -> None:
        if event.button() == Qt.RightButton:
            self.draw_finished.emit(True)
            self.iface.actionPan().trigger()
            return
        point = self.toMapCoordinates(event.pos())
        self.on_new_point(point)
        self.sketch_rubber_band.reset()
        self.sketch_rubber_band.addPoint(point)

    def canvasMoveEvent(self, event: QgsMapMouseEvent) -> None:
        if len(self.points) == 0:
            return
        self.sketch_rubber_band.removeLastPoint()
        point = self.toMapCoordinates(event.pos())
        self.sketch_rubber_band.addPoint(point)
