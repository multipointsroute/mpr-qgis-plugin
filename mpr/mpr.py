import os.path
from typing import List

from PyQt5.QtWidgets import QToolBar
from qgis.core import QgsMapLayer, QgsPointXY, QgsProject, QgsVectorLayer
from qgis.gui import QgisInterface, QgsMapCanvas
from qgis.PyQt.QtCore import QCoreApplication, QSettings, QTranslator
from qgis.PyQt.QtWidgets import QAction

from mpr.draw_tool import LineMapTool

# Initialize Qt resources from file resources.py
from mpr.resources import *  # noqa
from mpr.utils import (
    CYCLING_MODE,
    DRIVING_MODE,
    WALKING_MODE,
    MprLogger,
    get_icon,
    load_from_metadata,
)

PLUGIN_NAME = "MultiPointsRoute"
PLUGIN_NAME_SHORT = "MPR"


class MultiPointsRoutePlugin:

    iface: QgisInterface
    canvas: QgsMapCanvas
    actions: List[QAction]
    toolbar: QToolBar

    # constructor
    def __init__(self, iface: QgisInterface):

        # Create a logger
        self.logger = MprLogger()
        # Save reference to the QGIS interface
        self.iface = iface
        self.canvas = iface.mapCanvas()
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        locale_path = os.path.join(self.plugin_dir, "i18n", "MultiPointsRoute_{}.qm".format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)
            QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.toolbar = self.iface.addToolBar(PLUGIN_NAME)
        self.toolbar.setObjectName(PLUGIN_NAME)

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        return QCoreApplication.translate("MultiPointsRoute", message)

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI"""

        # Cycling
        action_cycling = QAction(get_icon(f"{CYCLING_MODE}.svg"), CYCLING_MODE.title(), self.iface.mainWindow())
        action_cycling.triggered.connect(lambda _: self.draw_route(CYCLING_MODE))
        # Walking
        action_walking = QAction(get_icon(f"{WALKING_MODE}.svg"), WALKING_MODE.title(), self.iface.mainWindow())
        action_walking.triggered.connect(lambda _: self.draw_route(WALKING_MODE))
        # Driving
        action_driving = QAction(get_icon(f"{DRIVING_MODE}.svg"), DRIVING_MODE.title(), self.iface.mainWindow())
        action_driving.triggered.connect(lambda _: self.draw_route(DRIVING_MODE))

        # add actions to QGIS
        for action in [action_cycling, action_walking, action_driving]:
            self.iface.addPluginToWebMenu(PLUGIN_NAME, action)
            self.toolbar.addAction(action)
            self.actions.append(action)

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI"""
        for action in self.actions:
            self.iface.removePluginWebMenu(self.tr(PLUGIN_NAME), action)
            self.iface.removeToolBarIcon(action)
        del self.toolbar

    def draw_route(self, mode: str) -> None:
        drawer = LineMapTool(self.iface)
        drawer.draw_finished.connect(lambda: self.add_route(mode, drawer.points))
        self.iface.messageBar().pushMessage("Select your route points on the map, finish with right-click")
        self.canvas.setMapTool(drawer)

    def add_route(self, mode: str, points: List[QgsPointXY]) -> QgsVectorLayer:
        self.logger.info(f"Selected {len(points)} points")
        flat_map = lambda f, xs: [y for ys in xs for y in f(ys)]
        coords = flat_map(lambda p: [p.x(), p.y()], points)
        url = f"{load_from_metadata('custom', 'api')}/{mode}/route.geojson?{'&'.join([f'c={c}' for c in coords])}"
        layer = QgsVectorLayer(url, "route", "ogr")
        if layer.isValid():
            layer.loadNamedStyle(os.path.join(self.plugin_dir, "styles", "route.qml"), False, QgsMapLayer.Labeling | QgsMapLayer.Symbology)
            feat = next(layer.getFeatures())
            layer.setName("{} route [{:.2f} km] ({:.0f} min)".format(mode.title(), feat["distance"] / 1000, feat["duration"] / 60))
            QgsProject.instance().addMapLayer(layer)
            self.iface.messageBar().pushSuccess("Route", f"{mode.title()} route added to map")
        return layer
