import json
import os
from configparser import ConfigParser
from typing import Any

from PyQt5.QtGui import QIcon
from qgis.core import Qgis, QgsMessageLog

CYCLING_MODE = "cycling"
WALKING_MODE = "walking"
DRIVING_MODE = "driving"
MODES = [CYCLING_MODE, WALKING_MODE, DRIVING_MODE]


def get_icon_path(name: str) -> str:
    return f":/plugins/mpr/{name}"


def get_icon(name: str) -> QIcon:
    return QIcon(get_icon_path(name))


def load_from_metadata(section: str, attr: str, file: str = "metadata.txt") -> str:
    """Read a value from QGIS plugin's metadata file"""
    config = ConfigParser()
    config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), file), encoding="utf-8")
    return config[section][attr]


def pretty_format(data: Any) -> str:
    return json.dumps(data, sort_keys=True, indent=4, default=lambda d: str(d))


class MprLogger:
    """Singleton instance used to log to QGIS"""

    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls.logger = QgsMessageLog()
            cls._instance = super().__new__(cls)
        return cls._instance

    def log(self, message: Any, level: Qgis.Info = Qgis.Info, **kwargs) -> None:
        log_tag = kwargs.pop("tag", "MPR")
        self.logger.logMessage(str(message), log_tag, level)

    def pretty_log(self, message: Any, level: Qgis.Info = Qgis.Info, **kwargs):
        self.log(pretty_format(message), level, **kwargs)

    def info(self, message: Any, **kwargs) -> None:
        self.log(message, Qgis.Info, **kwargs)

    def success(self, message: Any, **kwargs) -> None:
        self.log(message, Qgis.Success, **kwargs)

    def warning(self, message: Any, **kwargs) -> None:
        self.log(message, Qgis.Warning, **kwargs)

    def error(self, error: Any, **kwargs) -> None:
        self.log(error, Qgis.Critical, **kwargs)
